#include <todo.hpp>
#include <QTextStream>
#include <QString>

static QTextStream QIn(stdin);
static QTextStream QOut(stdout);

int main(int argc, char *argv[])
{
	todo *t;
	if(argc == 2)
	{
		t = new todo(argv[1]);
	}
	else
	{
		exit(0);
	}
	bool quit = false;
	QString command;
	while(!quit)
	{
		QIn >> command;
		if(command == "new")
		{
			QString args, temp;
			QIn >> args;
			QIn >> temp;
			args += temp;
			QIn >> temp;
			args += temp;
			t->addTask(args);
		}
		if(command == "list")
		{
			QMap<int, QMap<int, Task>> statusOrder;
			auto tasks = t->tasks();
			for(int i = 0; i < t->tasks().size(); i++)
			{
				Task task = tasks[i];
				statusOrder[static_cast<int>(task.status())].insert(i, task);
			}

			for(Task::Status i = static_cast<Task::Status>(0); i < Task::Status::COUNT; i = static_cast<Task::Status>(static_cast<int>(i) + 1))
			{
				QOut << statusToString(i) << endl;
				foreach (int j, statusOrder[static_cast<int>(i)].keys()) {
					QOut << "\t" << j << "\t" << statusOrder[static_cast<int>(i)][j].title() << '\n';
				}
			}
		}
		if(command == "quit")
		{
			quit = true;
		}
	}
	delete t;
	return 0;
}
